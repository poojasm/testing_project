
 function problem1(inventory, id) {
  try{
    const totatlEntriesInInventory = inventory.length;
    //car details of id - 33 (any given id)

    for (let index = 0; index < totatlEntriesInInventory; index++) {
      const { id: carId, car_make, car_model, car_year } = inventory[index];
      if (carId === id) {
        return `Car ${carId} is a ${car_year} ${car_make} ${car_model}`;
      }
    }
    
    return `Car with id:${id} not found in the data`;
  }

  catch(error){
    console.log("Error");
  }
}

export default problem1;
