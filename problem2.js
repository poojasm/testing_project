 function problem2(inventory) {
    try{
      const totatlEntriesInInventory = inventory.length;
      //returning car_make and car_model data of last entry in inventory

      if (totatlEntriesInInventory > 0) {
        const {car_make, car_model} = inventory[totatlEntriesInInventory - 1];
        return `Last car is a ${car_make} ${car_model}`;
      }
    }

    catch(error){
      console.log("Error")
    }
  }

  export default problem2;