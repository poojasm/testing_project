import problem4 from "./problem4.js";

function problem5(inventory) {
  try{
    const years = problem4(inventory);
    let noOfCarsOlderThan_2000 = 0;

    for (let index = 0; index < years.length; index++) {
      if (years[index] < 2000) {
        noOfCarsOlderThan_2000++;
      }
    }

    return noOfCarsOlderThan_2000;
  }
  catch(error){
    console.log("Error")
  }
  
}

export default problem5;
