function problem3(inventory) {
  try{
    let listedByCarModel = [...inventory];
    //listing car_models alphabetically

    listedByCarModel.sort((car1, car2) => {
      const car1Model = car1.car_model.toLowerCase();
      const car2Model = car2.car_model.toLowerCase();

      if (car1Model < car2Model) {
        return -1;
      } 

      else if (car1Model > car2Model) {
        return 1;
      }
      
      return 0;
    });

    return listedByCarModel;
  }

  catch(error){
    console.log("Error")
  }
}

export default problem3;
