function problem4(inventory) {
  try{
    const totatlEntriesInInventory = inventory.length;
    const carYears = [];

    for (let index = 0; index < totatlEntriesInInventory; index++) {
      let year = inventory[index].car_year
      carYears.push(year) 
    }
  
    return carYears;
  }
  catch(error){
    console.log("Error")
  }
}

export default problem4;