function problem6(inventory) {
  try{
    const totatlEntriesInInventory = inventory.length;
    const searchableCars = new Set(["audi", "bmw"]);
    let bmwAudiCars = [];

    for (let index = 0; index < totatlEntriesInInventory; index++) {
      const carMake = inventory[index].car_make;

    if (searchableCars.has(carMake.toLowerCase()))
      bmwAudiCars.push(inventory[index]);
    }

    return bmwAudiCars;
  }
  catch(error){
    console.log("Error")
  }
  
}

export default problem6;
